<?php namespace Brm\Hamburguesas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateBrmHamburguesas extends Migration
{
    public function up()
    {
        Schema::create('brm_hamburguesas_', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre');
            $table->text('descripcion')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('brm_hamburguesas_');
    }
}
