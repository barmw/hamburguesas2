<?php namespace Brm\Hamburguesas\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateBrmHamburguesas extends Migration
{
    public function up()
    {
        Schema::table('brm_hamburguesas_', function($table)
        {
            $table->string('slug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('brm_hamburguesas_', function($table)
        {
            $table->dropColumn('slug');
        });
    }
}
