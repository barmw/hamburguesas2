<?php namespace Brm\Hamburguesas\Models;

use Model;

/**
 * Model
 */
class Hamburguesa extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;


    /**
     * @var string The database table used by the model.
     */
    public $table = 'brm_hamburguesas_';

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /** Relaciones */
    public $attachOne = [
        'poster' => 'System\Models\File'
    ];

    public $attachMany = [
        'galeria' => 'System\Models\File'
    ];

    }