<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www/themes/hamburguesas/partials/header.htm */
class __TwigTemplate_7350ce802f8aabce330e1a2ca6bbfce5e84822209b786a630df8dddd64a120ee extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!-- header-start -->
<header>
    <div class=\"header-area\">
        <div id=\"sticky-header\" class=\"main-header-area\">
            <div class=\"container-fluid p-0\">
                <div class=\"row align-items-center no-gutters\">
                    <div class=\"col-xl-5 col-lg-5\">
                        <div class=\"main-menu  d-none d-lg-block\">
                            <nav>
                                <ul id=\"navigation\">
                                    <li><a class=\"active\" href=\"";
        // line 11
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("home");
        echo "\">home</a></li>
                                    <li><a href=\"";
        // line 12
        echo $this->extensions['Cms\Twig\Extension']->pageFilter("hamburguesas");
        echo "\">Menu</a></li>
                                    <li><a href=\"#\">Acerca</a></li>
                                    <li><a href=\"#\">blog <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"#\">blog</a></li>
                                            <li><a href=\"#\">single-blog</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"#\">Pages <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"#\">elements</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"#\">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-xl-2 col-lg-2\">
                        <div class=\"logo-img\">
                            <a href=\"index.html\">
                                <img src=\"";
        // line 33
        echo $this->extensions['Cms\Twig\Extension']->themeFilter("/assets/img/logo.png");
        echo "\" alt=\"\">
                            </a>
                        </div>
                    </div>
                    <div class=\"col-xl-5 col-lg-5 d-none d-lg-block\">
                        <div class=\"book_room\">
                            <div class=\"socail_links\">
                                <ul>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-instagram\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-twitter\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-facebook\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-google-plus\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class=\"book_btn d-none d-xl-block\">
                                <a class=\"#\" href=\"#\">+10 367 453 7382</a>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"mobile_menu d-block d-lg-none\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->

<!-- slider_area_start -->
<div class=\"slider_area\">
    <div class=\"slider_active owl-carousel\">
        <div class=\"single_slider  d-flex align-items-center slider_bg_1 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"single_slider  d-flex align-items-center slider_bg_2 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www/themes/hamburguesas/partials/header.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 33,  53 => 12,  49 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!-- header-start -->
<header>
    <div class=\"header-area\">
        <div id=\"sticky-header\" class=\"main-header-area\">
            <div class=\"container-fluid p-0\">
                <div class=\"row align-items-center no-gutters\">
                    <div class=\"col-xl-5 col-lg-5\">
                        <div class=\"main-menu  d-none d-lg-block\">
                            <nav>
                                <ul id=\"navigation\">
                                    <li><a class=\"active\" href=\"{{ 'home'|page }}\">home</a></li>
                                    <li><a href=\"{{ 'hamburguesas'|page }}\">Menu</a></li>
                                    <li><a href=\"#\">Acerca</a></li>
                                    <li><a href=\"#\">blog <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"#\">blog</a></li>
                                            <li><a href=\"#\">single-blog</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"#\">Pages <i class=\"ti-angle-down\"></i></a>
                                        <ul class=\"submenu\">
                                            <li><a href=\"#\">elements</a></li>
                                        </ul>
                                    </li>
                                    <li><a href=\"#\">Contact</a></li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class=\"col-xl-2 col-lg-2\">
                        <div class=\"logo-img\">
                            <a href=\"index.html\">
                                <img src=\"{{ '/assets/img/logo.png'|theme }}\" alt=\"\">
                            </a>
                        </div>
                    </div>
                    <div class=\"col-xl-5 col-lg-5 d-none d-lg-block\">
                        <div class=\"book_room\">
                            <div class=\"socail_links\">
                                <ul>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-instagram\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-twitter\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-facebook\"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"#\">
                                            <i class=\"fa fa-google-plus\"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class=\"book_btn d-none d-xl-block\">
                                <a class=\"#\" href=\"#\">+10 367 453 7382</a>
                            </div>
                        </div>
                    </div>
                    <div class=\"col-12\">
                        <div class=\"mobile_menu d-block d-lg-none\"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->

<!-- slider_area_start -->
<div class=\"slider_area\">
    <div class=\"slider_active owl-carousel\">
        <div class=\"single_slider  d-flex align-items-center slider_bg_1 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class=\"single_slider  d-flex align-items-center slider_bg_2 overlay\">
            <div class=\"container\">
                <div class=\"row align-items-center justify-content-center\">
                    <div class=\"col-xl-9 col-md-9 col-md-12\">
                        <div class=\"slider_text text-center\">
                            <div class=\"deal_text\">
                                <span>Big Deal</span>
                            </div>
                            <h3>Burger <br>
                                Bachelor</h3>
                            <h4>Maxican</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider_area_end -->", "C:\\wamp64\\www/themes/hamburguesas/partials/header.htm", "");
    }
}
