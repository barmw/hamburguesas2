<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* C:\wamp64\www/themes/hamburguesas/partials/footer.htm */
class __TwigTemplate_18877dfd1d7248060136097de6d74b13a9b4d34bb23b66808e825683847b250a extends \Twig\Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<footer class=\"footer\">
    <div class=\"footer_top\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xl-4 col-md-6 col-lg-4\">
                    <div class=\"footer_widget text-center \">
                        <h3 class=\"footer_title pos_margin\">
                                New York
                        </h3>
                        <p>5th flora, 700/D kings road, <br> 
                                green lane New York-1782 <br>
                                <a href=\"#\">info@burger.com</a></p>
                        <a class=\"number\" href=\"#\">+10 378 483 6782</a>

                    </div>
                </div>
                <div class=\"col-xl-4 col-md-6 col-lg-4\">
                    <div class=\"footer_widget text-center \">
                        <h3 class=\"footer_title pos_margin\">
                            California
                        </h3>
                        <p>5th flora, 700/D kings road, <br> 
                                green lane New York-1782 <br>
                                <a href=\"#\">info@burger.com</a></p>
                        <a class=\"number\" href=\"#\">+10 378 483 6782</a>

                    </div>
                </div>
                <div class=\"col-xl-4 col-md-12 col-lg-4\">
                        <div class=\"footer_widget\">
                                <h3 class=\"footer_title\">
                                        Stay Connected
                                </h3>
                                <form action=\"#\" class=\"newsletter_form\">
                                    <input type=\"text\" placeholder=\"Enter your mail\">
                                    <button type=\"submit\">Sign Up</button>
                                </form>
                                <p class=\"newsletter_text\">Stay connect with us to get exclusive offer!</p>
                            </div>
                </div>
            </div>
            <div class=\"row justify-content-center\">
                <div class=\"col-lg-4\">
                    <div class=\"socail_links text-center\">
                            <ul>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-instagram\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-twitter-alt\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-facebook\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"fa fa-google-plus\"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"copy-right_text\">
        <div class=\"container\">
            <div class=\"footer_border\"></div>
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <p class=\"copy_right text-center\">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "C:\\wamp64\\www/themes/hamburguesas/partials/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<footer class=\"footer\">
    <div class=\"footer_top\">
        <div class=\"container\">
            <div class=\"row\">
                <div class=\"col-xl-4 col-md-6 col-lg-4\">
                    <div class=\"footer_widget text-center \">
                        <h3 class=\"footer_title pos_margin\">
                                New York
                        </h3>
                        <p>5th flora, 700/D kings road, <br> 
                                green lane New York-1782 <br>
                                <a href=\"#\">info@burger.com</a></p>
                        <a class=\"number\" href=\"#\">+10 378 483 6782</a>

                    </div>
                </div>
                <div class=\"col-xl-4 col-md-6 col-lg-4\">
                    <div class=\"footer_widget text-center \">
                        <h3 class=\"footer_title pos_margin\">
                            California
                        </h3>
                        <p>5th flora, 700/D kings road, <br> 
                                green lane New York-1782 <br>
                                <a href=\"#\">info@burger.com</a></p>
                        <a class=\"number\" href=\"#\">+10 378 483 6782</a>

                    </div>
                </div>
                <div class=\"col-xl-4 col-md-12 col-lg-4\">
                        <div class=\"footer_widget\">
                                <h3 class=\"footer_title\">
                                        Stay Connected
                                </h3>
                                <form action=\"#\" class=\"newsletter_form\">
                                    <input type=\"text\" placeholder=\"Enter your mail\">
                                    <button type=\"submit\">Sign Up</button>
                                </form>
                                <p class=\"newsletter_text\">Stay connect with us to get exclusive offer!</p>
                            </div>
                </div>
            </div>
            <div class=\"row justify-content-center\">
                <div class=\"col-lg-4\">
                    <div class=\"socail_links text-center\">
                            <ul>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-instagram\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-twitter-alt\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"ti-facebook\"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href=\"#\">
                                        <i class=\"fa fa-google-plus\"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
    </div>
    <div class=\"copy-right_text\">
        <div class=\"container\">
            <div class=\"footer_border\"></div>
            <div class=\"row\">
                <div class=\"col-xl-12\">
                    <p class=\"copy_right text-center\">
                        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class=\"fa fa-heart-o\" aria-hidden=\"true\"></i> by <a href=\"https://colorlib.com\" target=\"_blank\">Colorlib</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>", "C:\\wamp64\\www/themes/hamburguesas/partials/footer.htm", "");
    }
}
